package notificacion;

import observer.Evento;
import observer.Observador;

public class ObservadorEvento implements Observador  {
	
	@Override
	public void update(Evento evento) {
		System.out.println("El paciente " + evento.getPaciente().getNombre() + " paso de " + evento.getEstadoAnterior() + " a "
			+ evento.getEstadoActual());		
	}
}
